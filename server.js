// eslint-disable
const express = require('express')
const app = express()
const history = require('connect-history-api-fallback')

// Make sure hitting refresh will hit index.html file
app.use(express.static('dist'))
app.use(history())
app.use(express.static('dist'))
// serve from
app.get('/', function(req, res) {
  res.sendFile(__dirname + '/')
})

const server = app.listen(80, function() {
  const port = server.address().port
  console.log('App listening', port)
})