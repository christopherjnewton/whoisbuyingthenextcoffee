import { shallowMount } from '@vue/test-utils'
import Confetti from '@/components/Confetti.vue'
describe('Confetti component', () => {
  const wrapper = shallowMount(Confetti)
  it('renders the correct markup', () => {
    expect(wrapper.html()).toContain('<canvas id="canvas"></canvas>')
  })
  it('match snapshot', () => {
    expect(wrapper.html()).toMatchSnapshot()
  })
  test('is a Vue instance', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
