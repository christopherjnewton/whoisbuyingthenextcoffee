import { shallowMount } from '@vue/test-utils'
import Coffee from '@/components/Coffee.vue'
describe('Coffee component', () => {
  const wrapper = shallowMount(Coffee)
  test('is a Vue instance', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
