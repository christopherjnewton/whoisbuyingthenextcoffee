# Who is buying the next coffee

## Build Setup

``` bash
# install dependencies
yarn

# serve with hot reload at localhost:8080
yarn serve

# build for production with minification
yarn build
```

## Framework

* [Element IO](http://element.eleme.io/#/en-US) for styled components
* [date-fns](https://date-fns.org/) for time conversions
* [lodash](https://lodash.com/) for misc handling of data

## Structure

* `src/api/Coffee.js` Coffee class to post / get results
  * Uses .env.production or .env.development depending on the script being run from cli
* `src/components/*` visual components for coffee and confetti
* `src/views/*` page layout container, houses methods
