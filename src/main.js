import Vue from 'vue'
import App from './App.vue'
import router from './router/index.js'
import Element from 'element-ui'
import VueChartkick from 'vue-chartkick'
import Highcharts from 'highcharts'

Vue.use(VueChartkick, { adapter: Highcharts })

Vue.use(Element)
Vue.use(router)

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
