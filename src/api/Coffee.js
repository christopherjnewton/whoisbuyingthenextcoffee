class CoffeeApi {
  constructor() {
		this.url = process.env.VUE_APP_ROOT_API; // eslint-disable-line
		this.key = process.env.VUE_APP_ROOT_API_KEY; // eslint-disable-line
  }
  getUsers() {
    return new Promise((resolve, reject) => {
      fetch(this.url + 'user', {
        method: 'GET',
        headers: new Headers({
          Authorization: 'Basic ' + btoa(this.key + ':'),
          Accept: 'application/json',
          'Content-Type': 'application/json',
        }),
      })
        .then(res => res.json())
        .then(data => {
          resolve(data)
          return data
        })
        .catch(response => {
          reject(response)
        })
    })
  }
  updateUsers(user) {
    return new Promise((resolve, reject) => {
      fetch(`${this.url}user/${user.id}`, {
        method: 'PUT',
        body: JSON.stringify({
          name: user.name,
          defaultAfternoon: user.default.afternoon,
          defaultMorning: user.default.morning,
        }),
        headers: new Headers({
          Authorization: 'Basic ' + btoa(this.key + ':'),
          Accept: 'application/json',
          'Content-Type': 'application/json',
        }),
      })
        .then(res => res.json())
        .then(data => {
          resolve(data)
        })
        .catch(response => {
          reject(response)
        })
    })
  }
  postCoffee(purchaser, checkedUsersEmail) {
    return new Promise((resolve, reject) => {
      fetch(this.url + 'coffee', {
        method: 'POST',
        body: JSON.stringify({
          purchaser: purchaser,
          users: checkedUsersEmail,
        }),
        headers: new Headers({
          Authorization: 'Basic ' + btoa(this.key + ':'),
          Accept: 'application/json',
          'Content-Type': 'application/json',
        }),
      })
        .then(res => res.json())
        .then(data => {
          resolve(data)
          return data
        })
        .catch(response => {
          reject(response)
        })
    })
  }
}

export default new CoffeeApi()
